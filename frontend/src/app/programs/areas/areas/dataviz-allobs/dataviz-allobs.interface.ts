export interface datavizSelection {
	species: Array<any>,
	years: Array<any>,
	stages: Array<any>,
	mountains: Array<any>
}
