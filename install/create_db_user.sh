# Taken from GeoNature installation
echo "Check Citizen database user '$user_pg' exists…"
user_pg_exists=$(psql -d postgres -t -c "SELECT COUNT(1) FROM pg_catalog.pg_roles WHERE  rolname = '${user_pg}';")
if [ ${user_pg_exists} -eq 0 ]; then
  echo "Create Citizen database user…"
  psql -d postgres -c "CREATE ROLE $user_pg WITH LOGIN PASSWORD '$user_pg_pass';"
fi
# Always grant privileges on the newly created database
psql -d postgres  -c "GRANT ALL PRIVILEGES ON DATABASE ${pg_dbname} TO ${user_pg};"
