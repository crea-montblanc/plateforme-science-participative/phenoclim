BACKOFFICE_USERNAME="${backoffice_username:-citizen}"
if [ ${backoffice_password:=MotDePasseAChanger} = MotDePasseAChanger ]; then
  backoffice_password=$(
    date +%s | sha256sum | base64 | head -c 30
    echo
  )
fi

cat > ${DIR}/config/backoffice_access <<- EOF
Backoffice password
===================
url: (${URL}/api/admin)
username: ${BACKOFFICE_USERNAME}
password: ${backoffice_password}
EOF
  htpasswd -b -c ${DIR}/config/backoffice_htpasswd ${BACKOFFICE_USERNAME} ${backoffice_password}
